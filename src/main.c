#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

void test1(){
    void *mem1 = _malloc(250);
    printf("\nTest1\n");
    debug_heap(stdout, HEAP_START);
    _free(mem1);
}

void test2()
{
    void *mem1 = _malloc(10);
    void *mem2 = _malloc(20);
    void *mem3 = _malloc(30);
    printf("\nTest2\n");
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
    _free(mem3);
}

void test3(){
    
    void *mem1 = _malloc(20);
    void *mem2 = _malloc(30);
    void *mem3 = _malloc(40);
    _free(mem2);
    printf("\nTest3\n");
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem3);
}

void test4(){
    void *mem1 = _malloc(2000);
    printf("\nTest4\n");
    debug_heap(stdout, HEAP_START);
    _free(mem1);
}

void test5(){
    void *mem1 = _malloc(2000);
    void *mem2 = _malloc(2000);
    printf("\nTest5\n");
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
}

int main(){
    
    heap_init(600);

    void (*tests[6]) () = {test1, test2, test3, test4, test5};

    for(long unsigned int i=0; i<5; i++){
        tests[i]();
    }

    return 0;
}
